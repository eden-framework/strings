module gitee.com/eden-framework/strings

go 1.16

require (
	gitee.com/eden-framework/reflectx v0.0.3
	github.com/sirupsen/logrus v1.7.0
)
